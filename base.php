<?php get_template_part('templates/head'); ?>
<body <?php body_class('sjublogs'); ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    // Use Bootstrap's navbar if enabled in config.php
    if (current_theme_supports('bootstrap-top-navbar')) {
      get_template_part('templates/header-top-navbar');
    } else {
      get_template_part('templates/header');
    }
  ?>
<div class="page-wrap container">
  <div class="page-content content row" role="document">
    <main class="main <?php echo roots_main_class(); ?>" role="main">
      <?php include roots_template_path(); ?>
    </main><!-- /.main -->
    <?php if (roots_display_sidebar()) : ?>
      <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
        <?php // if it's a single blog entry, we should show the author heading
        if(is_single()):
          if( get_field('post_writer') ):
            $post_object = get_field('post_writer');
            // override $post
            $post = $post_object[0];
            setup_postdata( $post ); 
            get_template_part('templates/author-widget');
            wp_reset_postdata();
          endif;
        endif;  
        ?>
        <?php include roots_sidebar_path(); ?>
      </aside><!-- /.sidebar -->
    <?php endif; ?>
  </div><!-- /.content -->
</div>
  <?php get_template_part('templates/footer'); ?>

</body>
</html>