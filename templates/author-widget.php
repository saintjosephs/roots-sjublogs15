<section class="author-widget">
  <figure class="author-photo">
    <?php the_post_thumbnail(); ?>
  </figure>
  <div class="author-info">
    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/author-fields'); ?>
  </div>
  <div class="author-readmore clearfix">
    <a class="btn btn-default pull-right" href="<?php the_permalink(); ?>">Read more by this author <i class="fa fa-arrow-right"></i></a>
  </div>
</section>