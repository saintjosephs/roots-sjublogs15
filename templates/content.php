<article <?php post_class('clearfix archive-post'); ?>>
  <div class="excerpt-details">
    <figure class="author-headshot">
    <?php if( get_field('post_writer') ):
      $post_object = get_field('post_writer');
      // override $post
      $post = $post_object[0];
      setup_postdata( $post ); 
      if( get_field('author_headshot') ):
        echo wp_get_attachment_image( get_field('author_headshot'), 'thumbnail');
        ?>
        <?php
      
      endif; 
      $authorid = get_the_id();
      wp_reset_postdata();
    endif;
    ?>
    </figure>
    <div class="byline author vcard"><a href="<?php echo get_post_permalink($authorid);?>"><em class="icon icon-user"></em> <?php echo get_the_title($authorid)?></a></div>
    <time class="published" datetime="<?php echo get_the_time('c'); ?>"><em class="icon icon-time"></em> <?php echo get_the_date(); ?></time>
    
    <div class="excerpt-details-comments"><a href="<?php comments_link(); ?>"><em class="icon icon-comments"></em> <?php comments_number('Add a comment!', '1 comment.', '% comments'); ?></a></div>
  </div>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>

</article>
