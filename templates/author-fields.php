<?php // list of fields reused throughout the site 
      // related to the author
      ?>
      
<?php if( get_field('graduation_year') ): ?>
<p><strong>Class Year:</strong> <?php the_field('graduation_year') ?></p>
<?php endif; ?>
<?php if( get_field('hometown') ): ?>
<p><strong>Hometown:</strong> <?php the_field('hometown') ?></p>
<?php endif; ?>
<?php if( get_field('job-title') ): ?>
<p><strong>Title:</strong> <?php the_field('job-title') ?></p>
<?php endif; ?>
<?php if( get_field('major') ): ?>
<p><strong>Area of Study:</strong> <?php the_field('major') ?></p>
<?php endif; ?>
<?php if( get_field('job_title') ): ?>
<p><strong>Specialization:</strong> <?php the_field('job_title') ?></p>
<?php endif; ?>
<?php if( get_field('interests') ): ?>
<p><strong>Interests:</strong> <?php the_field('interests') ?></p>
<?php endif; ?>