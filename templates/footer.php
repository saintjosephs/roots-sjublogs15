<footer class="site-footer clearfix">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 address">
          <div class="inner u-cf">
            <div class="logo-area"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/sju_white.png" alt="Saint Joseph's University" class="logo"></div> 
            <div class="address-area">
              <p><a href="http://www.sju.edu">Saint Joseph's University</a><br>
             5600 City Ave<br>
              Philadelphia, PA 19131<br>
               610-660-1000</p>
            </div>
          </div> 
        </div> 
        <div class="col-sm-4 footer-social"> 
          <div class="inner"> 
            <ul> 
              <li><a href="https://www.facebook.com/sjuadmissions"><i class="fa fa-facebook-square"></i> Facebook</a></li> 
              <li><a href="https://twitter.com/sjuadmissions"><i class="fa fa-twitter-square"></i> Twitter</a></li> 
              <li><a href="http://instagram.com/sjuadmissions"><i class="fa fa-instagram"></i> Instagram</a></li> 
            </ul> 
          </div> 
        </div> 
      </div>
    </div>
    
    <div class="shadow-mid"></div>
    <div class="shadow-ground"></div>
    
    
</footer>


<?php wp_footer(); ?>