<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
      <?php if ( has_post_thumbnail() ) {
        the_post_thumbnail('featured-img');
        }
      ?>
    </header>
    <div class="entry-content">
      <?php 
        if ( function_exists( 'sharing_display' ) ) {
          sharing_display( '', true );
        }
 
        if ( class_exists( 'Jetpack_Likes' ) ) {
          $custom_likes = new Jetpack_Likes;
          echo $custom_likes->post_likes( '' );
        }
        ?>
      <?php the_content(); ?>
      
      <?php if (has_category()): ?>
      <div class="post-categories"><strong>Posted in: </strong> <?php printf(get_the_category_list( __( ', ' ) )); ?></p></div>
      <?php endif; ?>
      <div class="post-tags"><p><em class="icon icon-archive"></em> <em class="icon icon-tags"></em> <?php printf(get_the_tag_list( '', __( ', ' ) )); ?></p></div>
      

    </div>
    <footer>
      
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
