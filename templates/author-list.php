<article class="author-list media">
  <figure class="pull-left">
    <?php 
    if( get_field('author_headshot') ):
      echo wp_get_attachment_image( get_field('author_headshot'), array(75, 75));      
    else:
      the_post_thumbnail('thumbnail', array('class' => 'media-object')); 
    endif;
    ?>
  </figure>
  <div class="author-info media-body">
    <h4><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
   <?php get_template_part('templates/author-fields'); ?>

  </div>
</article>