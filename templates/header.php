<header id="banner" class="site-header">

  
  
  <div id="top-bar">
    <section class="container">
      <div class="site-header-content">
        <figure class="site-logo">
          <a href="/"><img src="http://www.sju.edu/sites/default/files/sju-circlehead.png" alt="Saint Joseph's University"></a>
        </figure>
        <div class="blogs-brand">
          <h1><a class="brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></h1>
        </div>
      </div>
      
    </section>
  </div>
  
  
</header>

<header class="navbar navbar-inverse" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand visible-xs" href="<?php echo home_url(); ?>/">Menu</a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
        endif;
      ?>
    </nav>
  </div>
</header>
<div class="imageBackground clearfix"></div>