<section class="author-header">
  <div class="container">
    <div class="row">
      <figure class="author-photo col-sm-7">
        <?php the_post_thumbnail(); ?>
      </figure>
      <div class="author-info col-sm-5">
        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
        <?php get_template_part('templates/author-fields'); ?>
      </div>
    </div>
  </div>
</section>
