<div class="row">
<?php get_template_part('templates/author-header'); ?>
</div> 
<?php while (have_posts()) : the_post(); ?>
<section>
  <div class="author-bio">
    <h1>About</h1>
    <?php the_content(); ?>
  </div>
  <div class="author-posts">
    <h1>Posts</h1>
    
    <?php 
    $theauthor = get_the_ID();
    // args
    $args = array(
    	'posts_per_page' => -1,
    	'post_type' => 'post',
    	'meta_query' => array(
						array(
							'key' => 'post_writer', // name of custom field
							'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
							'compare' => 'LIKE'
						)
					)
    );
    // get results
    $the_query = new WP_Query( $args );
    // The Loop
    ?>
    <?php if( $the_query->have_posts() ): ?>
    	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    	<article <?php post_class('clearfix'); ?>>
            
            <div class="">
              <header>
                <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
              </header>
              <div class="entry-summary">
                
                <?php the_excerpt(); ?>
              </div>
              <footer>
                <div class="entry-meta row">
                  <div class="byline author vcard col-sm-4"><a href="<?php get_permalink($theauthor);?>"><em class="icon icon-user"></em> <?php echo get_the_title($theauthor)?></a></div>
                  <time class="published col-sm-4" datetime="<?php echo get_the_time('c'); ?>"><em class="icon icon-time"></em> <?php echo get_the_date(); ?></time>
                  <div class="comments col-sm-4">
                    <a href="<?php comments_link(); ?>"><em class="icon icon-comments"></em> <?php comments_number('Be the first to comment!', '1 comment.', '% comments'); ?></a>
                  </div>
                </div>
              </footer>
            </div>
          </article>
    	<?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
  </div>
</section>
<?php endwhile; ?>