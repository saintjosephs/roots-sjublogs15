<?php get_template_part('templates/page', 'header'); ?>
<!-- make the list of all the writers -->
<div class="full-author-list">
<?php 
$args = array(
	'post_type' => 'sjublogs_writer',
	'posts_per_page'   => -1,
	'offset' => 0,
	'orderby'          => 'title',
	'order'            => 'ASC',
	'meta_key'		=> 'active_blogger',
	'meta_value'	=> '1'
 );
$postslist = get_posts( $args );

?>
<?php
foreach ( $postslist as $post ) :
  setup_postdata( $post );  
  get_template_part('templates/author-list');	
endforeach; 
wp_reset_postdata();
?>
</div>
<h2>Past Bloggers</h2>
<div class="full-author-list">
<?php 
$args = array(
	'post_type' => 'sjublogs_writer',
	'posts_per_page'   => -1,
	'offset' => 0,
	'orderby'          => 'title',
	'order'            => 'ASC',
	'meta_key'		=> 'active_blogger',
	'meta_value'	=> '0'
 );
$postslist = get_posts( $args );

?>
<?php
foreach ( $postslist as $post ) :
  setup_postdata( $post );  
  get_template_part('templates/author-list');	
endforeach; 
wp_reset_postdata();
?>
</div>