<?php
/**
 * Custom functions
 */

// love to http://generatewp.com/post-type/
// Register Custom Post Type
function sjublogs_writer() {

	$labels = array(
		'name'                => _x( 'Writers', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Writer', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Writer', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Writer:', 'text_domain' ),
		'all_items'           => __( 'All Writers', 'text_domain' ),
		'view_item'           => __( 'View Writer', 'text_domain' ),
		'add_new_item'        => __( 'Add New Writer', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Writer', 'text_domain' ),
		'update_item'         => __( 'Update Writer', 'text_domain' ),
		'search_items'        => __( 'Search Writers', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                => 'writer',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'sjublogs_writer', 'text_domain' ),
		'description'         => __( 'Writer Information for SJU Blogs', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', ),
		'taxonomies'          => array( 'category' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'sjublogs_writer', $args );

}

// Hook into the 'init' action
add_action( 'init', 'sjublogs_writer', 0 );


add_filter('roots_wrap_base', 'roots_wrap_base_cpts'); // Add our function to the roots_wrap_base filter

  function roots_wrap_base_cpts($templates) {
    $cpt = get_post_type(); // Get the current post type
    if ($cpt) {
       array_unshift($templates, 'base-' . $cpt . '.php'); // Shift the template to the front of the array
    }
    return $templates; // Return our modified array with base-$cpt.php at the front of the queue
  }
  
/**
 * Add support for Vertical Featured Images
 */
if ( ! function_exists( 'mytheme_vertical_check' ) ) :
	function mytheme_vertical_check( $html, $post_id, $post_thumbnail_id, $size, $attr ) {
 
		$image_data = wp_get_attachment_image_src( $post_thumbnail_id , 'large' );
 
		//Get the image width and height from the data provided by wp_get_attachment_image_src()
		$width  = $image_data[1];
		$height = $image_data[2];
 
		if ( $height > $width ) {
			$html = str_replace( 'attachment-', 'vertical-image attachment-', $html );
		}
		return $html;
	}
endif;
 
add_filter( 'post_thumbnail_html', 'mytheme_vertical_check', 10, 5 );