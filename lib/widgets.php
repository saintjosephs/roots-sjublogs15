<?php
/**
 * Register sidebars and widgets
 */
function roots_widgets_init() {
  // Sidebars
  register_sidebar(array(
    'name'          => __('Primary', 'roots'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));

  
  register_sidebar(array(
    'name'          => __('Footer Left', 'roots'),
    'id'            => 'sidebar-footer-left',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));
  register_sidebar(array(
    'name'          => __('Footer', 'roots'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));
  register_sidebar(array(
    'name'          => __('Footer Right', 'roots'),
    'id'            => 'sidebar-footer-right',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));

  // Widgets
  register_widget('Roots_Vcard_Widget');
}
add_action('widgets_init', 'roots_widgets_init');

/**
 * Example vCard widget
 */
class Roots_Vcard_Widget extends WP_Widget {
  private $fields = array(
    'title'          => 'Title (optional)',
    'street_address' => 'Street Address',
    'locality'       => 'City/Locality',
    'region'         => 'State/Region',
    'postal_code'    => 'Zipcode/Postal Code',
    'tel'            => 'Telephone',
    'email'          => 'Email'
  );

  function __construct() {
    $widget_ops = array('classname' => 'widget_roots_vcard', 'description' => __('Use this widget to add a vCard', 'roots'));

    $this->WP_Widget('widget_roots_vcard', __('Roots: vCard', 'roots'), $widget_ops);
    $this->alt_option_name = 'widget_roots_vcard';

    add_action('save_post', array(&$this, 'flush_widget_cache'));
    add_action('deleted_post', array(&$this, 'flush_widget_cache'));
    add_action('switch_theme', array(&$this, 'flush_widget_cache'));
  }

  function widget($args, $instance) {
    $cache = wp_cache_get('widget_roots_vcard', 'widget');

    if (!is_array($cache)) {
      $cache = array();
    }

    if (!isset($args['widget_id'])) {
      $args['widget_id'] = null;
    }

    if (isset($cache[$args['widget_id']])) {
      echo $cache[$args['widget_id']];
      return;
    }

    ob_start();
    extract($args, EXTR_SKIP);

    $title = apply_filters('widget_title', empty($instance['title']) ? __('vCard', 'roots') : $instance['title'], $instance, $this->id_base);

    foreach($this->fields as $name => $label) {
      if (!isset($instance[$name])) { $instance[$name] = ''; }
    }

    echo $before_widget;

    if ($title) {
      echo $before_title, $title, $after_title;
    }
  ?>
    <p class="vcard">
      <a class="fn org url" href="<?php echo home_url('/'); ?>"><?php bloginfo('name'); ?></a><br>
      <span class="adr">
        <span class="street-address"><?php echo $instance['street_address']; ?></span><br>
        <span class="locality"><?php echo $instance['locality']; ?></span>,
        <span class="region"><?php echo $instance['region']; ?></span>
        <span class="postal-code"><?php echo $instance['postal_code']; ?></span><br>
      </span>
      <span class="tel"><span class="value"><?php echo $instance['tel']; ?></span></span><br>
      <a class="email" href="mailto:<?php echo $instance['email']; ?>"><?php echo $instance['email']; ?></a>
    </p>
  <?php
    echo $after_widget;

    $cache[$args['widget_id']] = ob_get_flush();
    wp_cache_set('widget_roots_vcard', $cache, 'widget');
  }

  function update($new_instance, $old_instance) {
    $instance = array_map('strip_tags', $new_instance);

    $this->flush_widget_cache();

    $alloptions = wp_cache_get('alloptions', 'options');

    if (isset($alloptions['widget_roots_vcard'])) {
      delete_option('widget_roots_vcard');
    }

    return $instance;
  }

  function flush_widget_cache() {
    wp_cache_delete('widget_roots_vcard', 'widget');
  }

  function form($instance) {
    foreach($this->fields as $name => $label) {
      ${$name} = isset($instance[$name]) ? esc_attr($instance[$name]) : '';
    ?>
    <p>
      <label for="<?php echo esc_attr($this->get_field_id($name)); ?>"><?php _e("{$label}:", 'roots'); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id($name)); ?>" name="<?php echo esc_attr($this->get_field_name($name)); ?>" type="text" value="<?php echo ${$name}; ?>">
    </p>
    <?php
    }
  }
}



// Creating the widget 
class sjublogs_writers_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      // Base ID of your widget
      'sjublogs_writers_widget', 

      // Widget name will appear in UI
      __('SJU Blogs - List of Writers', 'sjublogs_writers_widget_domain'), 

      // Widget description
      array( 'description' => __( 'A list of all the writers', 'sjublogs_writers_widget_domain' ), ) 
    );
  }

  // Creating widget front-end
  // This is where the action happens
  public function widget( $args, $instance ) {
    
    $show_archived_users = $instance[ 'show_archived_users_checkbox' ] ? 'true' : 'false';
    $title = apply_filters( 'widget_title', $instance['title'] );
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
      echo $args['before_title'] . $title . $args['after_title'];
    
    
    // This is where you run the code and display the output
    echo('<ul class="writer-list-widget">');
    $loop = new WP_Query( array( 'post_type' => 'sjublogs_writer', 'posts_per_page' => -1, 'orderby'=> 'title', 'order' => 'ASC' ) );
    while ( $loop->have_posts() ) : $loop->the_post(); 
      
      // should we show the archived users?
      
      if($show_archived_users == 'true'):
        if(!sjublogs_isActiveWriter(get_the_ID())): ?>
          <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php 
        endif;

    
      // should we show the archived users?
      else:
        if(sjublogs_isActiveWriter(get_the_ID())): ?>
          <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php 
        endif;
      endif;
      
      ?>
      <?php
    endwhile; 
    wp_reset_query();
    echo('</ul>');
    
    
    echo $args['after_widget'];
  }
		
  // Widget Backend 
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    }
    else {
      $title = __( 'New title', 'sjublogs_writers_widget_domain' );
    }
  // Widget admin form
  ?>
  <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
  </p>
  
  <p>
    <input class="checkbox" type="checkbox" <?php checked( $instance[ 'show_archived_users_checkbox' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'show_archived_users_checkbox' ); ?>" name="<?php echo $this->get_field_name( 'show_archived_users_checkbox' ); ?>" /> 
    <label for="<?php echo $this->get_field_id( 'show_archived_users_checkbox' ); ?>">Display Inactive Writers (if checked, will ONLY display inactive writers)</label>
  </p>
  <!-- Remember to change 'your_checkbox_var' for your custom ID, as well -->
  <?php 
  }
	
  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance[ 'show_archived_users_checkbox' ] = $new_instance[ 'show_archived_users_checkbox' ];
    return $instance;
  }
} // Class wpb_widget ends here



// Register and load the widget
function sjublogs_writers_load_widget() {
	register_widget( 'sjublogs_writers_widget' );
}
add_action( 'widgets_init', 'sjublogs_writers_load_widget' );



/* Takes the ID of a writer and returns if they are active or not */
function sjublogs_isActiveWriter($writerID){
  if ( get_field( 'active_blogger', $writerID ) ):
    return TRUE;
  else:
    return FALSE;
  endif;
  
  return TRUE; // in case, for some reason, the active checker is broken (ACF Broken?)
}

